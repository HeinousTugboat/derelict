# Derelict

An incremental game about an abandoned ship, lost somewhere. [Link](https://heinoustugboat.gitlab.io/derelict/)

# Colophon

This is a bog-standard Angular app, generated with the Angular CLI and kept updated somewhat haphazardly by myself. The designs are all mine. I've decided to release this as GPL, in case anyone else is interested in it. Bug reports can be made [here](https://gitlab.com/HeinousTugboat/derelict/-/issues/new).

Finally, there is a small [wiki](https://gitlab.com/HeinousTugboat/derelict/-/wikis/home).

## Running locally

Nothing fancy:

```
git clone https://gitlab.com/HeinousTugboat/derelict.git
cd derelict
npm install
npm start -- --open
```
