import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { PowerService } from '@game/power.service';
import { Bot, BotJob } from '@models/bot';
import { PowerSource } from '@models/power-source';
import { invalid } from '@utils';

@Component({
  selector: 'app-bot-card',
  templateUrl: './bot-card.component.html',
  styleUrls: ['./bot-card.component.scss']
})
export class BotCardComponent implements OnInit {
  @Input() public bot?: Bot;
  @Output() public sourceDetached = new EventEmitter<PowerSource>();
  @Output() public sourceAttached = new EventEmitter<PowerSource>();

  public jobListOpen = false;
  public sourceListOpen = false;

  constructor(public power: PowerService) { }

  public ngOnInit(): void {
    if (!this.bot) {
      throw new Error('Bot Card instantiated without bot!');
    }
  }

  public openJobList(): void {
    this.jobListOpen = !this.jobListOpen;

  }

  public selectJob(job: BotJob | null): void {
    if (!this.bot) { throw new Error('Missing Bot'); }

    this.jobListOpen = false;
    if (!invalid(job)) {
      this.bot.job = job;
    }
  }

  public openSourceList(): void {
    this.sourceListOpen = !this.sourceListOpen;
  }

  public attachSource(source: PowerSource | null): void {
    if (!this.bot) { throw new Error('Missing Bot'); }

    this.sourceListOpen = false;
    if (!invalid(source)) {
      this.bot.source = source;
      this.sourceAttached.emit(source);
    }
  }

  public detachSource(): void {
    if (!this.bot) { throw new Error('Missing Bot'); }

    const source = this.bot.source;
    this.bot.source = undefined;
    this.sourceDetached.emit(source);
    console.log(source);
  }

}
