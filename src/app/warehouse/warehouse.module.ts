import { NgModule } from '@angular/core';
import { SharedModule } from '@shared/shared.module';

import { BotCardComponent } from './bot-card/bot-card.component';
import { WarehouseRoutingModule } from './warehouse-routing.module';
import { WarehouseComponent } from './warehouse.component';

@NgModule({
  declarations: [WarehouseComponent, BotCardComponent],
  imports: [
    SharedModule,
    WarehouseRoutingModule
  ]
})
export class WarehouseModule { }
