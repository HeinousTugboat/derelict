import { Component } from '@angular/core';
import { BotService } from '@game/bot.service';
import { PowerService } from '@game/power.service';
import { PowerSource } from '@models/power-source';

@Component({
  selector: 'app-warehouse',
  templateUrl: './warehouse.component.html',
  styleUrls: ['./warehouse.component.scss']
})
export class WarehouseComponent {
  constructor(
    public bot: BotService,
    public power: PowerService
  ) { }

  public detachedSource(source: PowerSource): void {
    this.power.addSource(source);
  }

  public attachedSource(source: PowerSource): void {
    this.power.removeSource(source);
  }

}
