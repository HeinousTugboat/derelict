import { NgModule } from '@angular/core';
import { SharedModule } from '@shared/shared.module';

import { EngineeringRoutingModule } from './engineering-routing.module';
import { EngineeringComponent } from './engineering.component';

@NgModule({
  declarations: [EngineeringComponent],
  imports: [
    SharedModule,
    EngineeringRoutingModule
  ]
})
export class EngineeringModule { }
