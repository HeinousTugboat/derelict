import { Component } from '@angular/core';
import { DecryptionService } from '@game/decryption.service';
import { Tech, TechCategory } from '@models/tech';

@Component({
  selector: 'app-engineering',
  templateUrl: './engineering.component.html',
  styleUrls: ['./engineering.component.scss']
})
export class EngineeringComponent {
  constructor(private decryption: DecryptionService) { }

  public get processingTechs(): Set<Tech> {
    return Tech.get(TechCategory.PROCESSING);
  }

  public get botTechs(): Set<Tech> {
    return Tech.get(TechCategory.BOT);
  }

  public get batteryTechs(): Set<Tech> {
    return Tech.get(TechCategory.BATTERY);
  }

  public get powerTechs(): Set<Tech> {
    return Tech.get(TechCategory.POWER);
  }

  public decrypt(tech: Tech): void {
    console.log(tech);
    // this.decryption.decrypt(tech);
  }
}
