import { Component, EventEmitter, Input, Output } from '@angular/core';

import { ProgressButtonModel } from './progress-button.model';

@Component({
  selector: 'app-progress-button',
  templateUrl: './progress-button.component.html',
  styleUrls: ['./progress-button.component.scss']
})
export class ProgressButtonComponent {
  // @Input() isRunning = false;
  // @Input() length = 0;
  // @Input() duration = 10;
  // @Input() canStart = false;
  // @Input() label = 'Start';
  @Input() data: ProgressButtonModel = {
    isRunning: false,
    length: 0,
    duration: 10,
    disabled: false,
    label: 'Start'
  };
  @Output() started = new EventEmitter();
}
