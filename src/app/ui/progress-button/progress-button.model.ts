export class ProgressButtonModel {
  public isRunning = false;
  public duration = 0;
  public disabled = false;
  constructor(public label: string, public length: number) { }
}
