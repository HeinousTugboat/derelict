import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-progress',
  templateUrl: './progress.component.html',
  styleUrls: ['./progress.component.scss']
})
export class ProgressComponent {
  @Input() public type = 'progress-bar';
  @Input() public value = 5;
  @Input() public min = 0;
  @Input() public max = 10;
  @Input() public low?: number;
  @Input() public high?: number;
  @Input() public optimum?: number;
}
