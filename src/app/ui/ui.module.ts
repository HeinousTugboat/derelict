import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { ProgressButtonComponent } from './progress-button/progress-button.component';
import { ProgressComponent } from './progress/progress.component';

@NgModule({
  declarations: [
    ProgressComponent,
    ProgressButtonComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    ProgressComponent,
    ProgressButtonComponent
  ]
})
export class UiModule { }
