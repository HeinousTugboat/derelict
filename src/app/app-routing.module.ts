import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ChangelogComponent } from './changelog/changelog.component';
import { CornucopiaModule } from './cornucopia/cornucopia.module';
import { DashboardModule } from './dashboard/dashboard.module';
import { EngineeringModule } from './engineering/engineering.module';
import { ProcessingModule } from './processing/processing.module';
import { WarehouseModule } from './warehouse/warehouse.module';

const routes: Routes = [
  {
    path: 'dashboard',
    loadChildren: (): Promise<DashboardModule> => import('./dashboard/dashboard.module').then(m => m.DashboardModule)
  },
  {
    path: 'warehouse',
    loadChildren: (): Promise<WarehouseModule> => import('./warehouse/warehouse.module').then(m => m.WarehouseModule)
  },
  {
    path: 'processing',
    loadChildren: (): Promise<ProcessingModule> => import('./processing/processing.module').then(m => m.ProcessingModule)
  },
  {
    path: 'cornucopia',
    loadChildren: (): Promise<CornucopiaModule> => import('./cornucopia/cornucopia.module').then(m => m.CornucopiaModule)
  },
  {
    path: 'engineering',
    loadChildren: (): Promise<EngineeringModule> => import('./engineering/engineering.module').then(m => m.EngineeringModule)
  },
  {
    path: 'changelog',
    component: ChangelogComponent
  },
  {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
