export interface Loop {
  interval: number;
  value: number;
  tick: number;
  debug: boolean;
  delay: number;
  offset: number;
}
