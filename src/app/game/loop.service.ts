import {
  animationFrameScheduler,
  ConnectableObservable,
  EMPTY,
  iif,
  interval as intervalObserver,
  Observable,
  Subject
} from 'rxjs';
import { map, publish, switchMap, takeWhile, tap, timeInterval } from 'rxjs/operators';

import { Injectable } from '@angular/core';

import { Loop } from './loop.interface';

@Injectable({
  providedIn: 'root'
})
export class LoopService {
  public loop$: ConnectableObservable<Loop>;

  private _tick = 0;
  private _time = performance.now();

  private readonly running$: Subject<boolean> = new Subject();
  private debug = true;
  private running = true;
  private loopSource$: Observable<Loop>;
  private delay = 0;
  private offset = 0;

  constructor() {
    this.loopSource$ = intervalObserver(0, animationFrameScheduler).pipe(
      takeWhile(() => this.running),
      timeInterval(animationFrameScheduler),
      map(({ interval }) => {
        if (interval > 1000) {
          this.delay += interval - 1000;
          return {
            interval: 1000,
            value: this._time,
            tick: this._tick,
            debug: this.debug,
            delay: this.delay,
            offset: this.offset
          };
        } else {
          if (this.delay > interval) {
            this.delay -= interval;
            return {
              interval: interval * 2,
              value: this._time,
              tick: this._tick,
              debug: this.debug,
              delay: this.delay,
              offset: this.offset
            };
          } else {
            this.offset = this.delay;
            this.delay = 0;
            return {
              interval: interval + this.offset,
              value: this._time,
              tick: this._tick,
              debug: this.debug,
              delay: this.delay,
              offset: this.offset
            };
          }
        }
      })
    );

    this.loop$ = this.running$.pipe(
      tap(x => console.info('running$', x)),
      switchMap(active => iif(() => active, this.loopSource$, EMPTY)),
      publish()
    ) as ConnectableObservable<Loop>;

    this.loop$.subscribe({
      next: ({ value, interval, delay, offset }) => {
        if (this._tick % 1000 === 0) {
          console.info(`game/loop tick! (${interval}ms) ${this._tick} : ${value}`);

          if (delay !== 0 || offset !== 0) {
            console.warn('delayed', delay, offset);
          }
        }

        // Should the game speed up invisibly or visibly? 🤔
        this._time += interval;
        // this._time = performance.now();
        ++this._tick;
      },
      error: (err) => console.error('game/loop error!', err),
      complete: () => console.info('game/loop complete!')
    });

    this.loop$.connect();
    this.running$.next(true);
  }

  public get tick(): number { return this._tick; }
  public get time(): number { return this._time; }
  public stop(): boolean { return this.running = false; }
  public pause(): void { this.running$.next(false); }
  public play(): void { this.running$.next(true); }
}
