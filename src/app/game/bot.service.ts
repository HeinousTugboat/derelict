import { Subscription } from 'rxjs';

import { Injectable, OnDestroy } from '@angular/core';
import { Battery } from '@models/battery';
import { Bot, BotJob } from '@models/bot';
import { PowerSource } from '@models/power-source';
import { assertNever } from '@utils';

import { LoopService } from './loop.service';
import { ResourceService } from './resource.service';

@Injectable({
  providedIn: 'root'
})
export class BotService implements OnDestroy {
  private _bots: Bot[] = [];
  private _power: PowerSource[] = [];
  private _work: Map<Bot, number> = new Map();

  private loopSub: Subscription;

  constructor(
    private loop: LoopService,
    private resource: ResourceService
  ) {
    const bot = new Bot(new Battery());
    this._bots.push(bot);
    this._work.set(bot, 0);

    this.loopSub = this.loop.loop$.subscribe(({ interval }) => {
      this.tick(interval);
    });
  }

  public get bots(): readonly Bot[] { return [...this._bots]; }
  public get power(): readonly PowerSource[] { return [...this._power]; }
  public get work(): number { return [...this._work.values()].reduce((acc, val) => acc + val, 0); }

  public ngOnDestroy(): void {
    this.loopSub.unsubscribe();
  }

  public tick(dT: number): void {
    for (const [bot, progress] of this._work.entries()) {
      let work = bot.tick(dT) + progress;

      switch (bot.job) {
        case BotJob.IDLE:
          work = 0;
          break;
        case BotJob.MINING_REGOLITH:
          while (work >= this.resource.regolith.workNeeded) {
            work -= this.resource.regolith.harvest(work);
          }
          break;
        default: assertNever(bot.job);
      }

      this._work.set(bot, work);
    }
  }

  public mine(j: number): void {
    if (j > 15) {
      this.resource.regolith.current += 1;
    }
  }

  public buildBot(): Bot {
    const newBot = new Bot(new Battery());
    this._bots.push(newBot);
    this._work.set(newBot, 0);
    return newBot;
  }
}
