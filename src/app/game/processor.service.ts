import { Subscription } from 'rxjs';

import { Injectable, OnDestroy } from '@angular/core';
import { MaterialInput, Processor } from '@models/processor.model';

import { LoopService } from './loop.service';
import { ResourceService } from './resource.service';

@Injectable({
  providedIn: 'root'
})
export class ProcessorService implements OnDestroy {
  private loopSub: Subscription;
  private processors: Processor[];

  constructor(private loop: LoopService, private resource: ResourceService) {
    this.loopSub = loop.loop$.subscribe(() => {
      this.tick();
    });

    this.processors = [
      new Processor([new MaterialInput(resource.regolith, 3)], resource.refinedRegolith)
    ];
  }

  public ngOnDestroy(): void {
    this.loopSub.unsubscribe();
  }

  public tick(): void {
    this.processors.forEach((processor) => processor.process());
  }
}
