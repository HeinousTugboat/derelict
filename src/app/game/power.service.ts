import { Subscription } from 'rxjs';

import { Injectable, OnDestroy } from '@angular/core';
import { Battery, isBattery } from '@models/battery';
import { PowerSource } from '@models/power-source';

import { LoopService } from './loop.service';

@Injectable({
  providedIn: 'root'
})
export class PowerService implements OnDestroy {
  private _sources: Set<PowerSource> = new Set();
  private _batteries: Set<Battery> = new Set();

  private loopSub: Subscription;

  constructor(
    private loop: LoopService
  ) {
    this.loopSub = loop.loop$.subscribe(({ interval }) => {
      this.tick(interval);
    });
  }

  public get sources(): readonly PowerSource[] { return [...this._sources]; }
  public get batteries(): readonly Battery[] { return [...this._batteries]; }

  public ngOnDestroy(): void {
    this.loopSub.unsubscribe();
  }

  public tick(dT: number): void {
    for (const battery of this._batteries) {
      battery.recharge(dT / 100);
    }
  }

  public addSource(source: PowerSource): void {
    this._sources.add(source);
    if (isBattery(source)) {
      this._batteries.add(source);
    }
  }

  public removeSource(source: PowerSource): void {
    this._sources.delete(source);
    if (isBattery(source)) {
      this._batteries.delete(source);
    }
  }
}
