import { Subscription } from 'rxjs';

import { Injectable, OnDestroy } from '@angular/core';
import { Resource } from '@models/resource';

import { LoopService } from './loop.service';

@Injectable({
  providedIn: 'root'
})
export class ResourceService implements OnDestroy {
  regolith = new Resource('regolith', 'Regolith', 0);
  refinedRegolith = new Resource('refined-regolith', 'Refined Regolith', 10, 20);
  mongrelAlloy = new Resource('mongrel-alloy', 'Mongrel Alloy', 0, 1);
  ceramicSlagOxides = new Resource('ceramic-slag-oxides', 'Ceramic Slag Oxides', 0);

  resources: Map<Resource, number> = new Map([
    [this.regolith, 0],
    [this.refinedRegolith, 0],
    [this.ceramicSlagOxides, 0],
    [this.mongrelAlloy, 0]
  ]);


  private loopSub: Subscription;

  constructor(private loop: LoopService) {
    this.loopSub = loop.loop$.subscribe(({ value }) => {
      this.tick(value);
    });
  }

  public ngOnDestroy(): void {
    this.loopSub.unsubscribe();
  }

  public work(resource: Resource, j: number): void {
    if (!this.resources.has(resource)) { throw new Error(`Attempting to work with missing Resource! ${resource}`); }
    const work = (this.resources.get(resource) ?? 0) + j;

    if (work >= resource.workNeeded) {
      const remainingWork = work - resource.harvest(work);
      this.resources.set(resource, remainingWork);
    } else {
      this.resources.set(resource, work);
    }
  }

  public tick(time: number): void {
    for (const resource of this.resources.keys()) {
      resource.updateRate(time);
    }
  }
}
