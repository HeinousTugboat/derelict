import { BehaviorSubject, ConnectableObservable } from 'rxjs';
import { publish, tap } from 'rxjs/operators';

import { Injectable } from '@angular/core';
import { DecryptedData, EncryptedTech, Tech } from '@models/tech';

@Injectable({
  providedIn: 'root'
})
export class DecryptionService {
  public decrypted$: ConnectableObservable<DecryptedData>;
  private decryptedData: DecryptedData = new Set([]);
  private decryptedSource$: BehaviorSubject<DecryptedData>;

  constructor() {
    console.log('decryption service ctor');
    Tech.initialize();
    const techs = Tech.load();

    console.log('decryption service', techs);

    this.decryptedSource$ = new BehaviorSubject(this.decryptedData);
    this.decrypted$ = this.decryptedSource$.pipe(
      tap(x => console.log('decrypted$', x)),
      publish()
    ) as ConnectableObservable<DecryptedData>;

    this.decrypted$.connect();
  }

  public decrypt(tech: EncryptedTech): boolean | null {
    if (!this.decryptedData.has(tech)) {
      this.decryptedData.add(tech);
      this.decryptedSource$.next(new Set(this.decryptedData));
      return true;
    }

    return false;
  }
}
