import { CommonModule } from '@angular/common';
import { NgModule, Optional, SkipSelf } from '@angular/core';

@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ]
})
export class GameModule {
  constructor(@Optional() @SkipSelf() parentModule: GameModule) {
    console.log(parentModule);
    if (parentModule !== null) {
      throw new Error(
        'GameModule is already loaded. Import it in the AppModule only');
    }
  }
}
