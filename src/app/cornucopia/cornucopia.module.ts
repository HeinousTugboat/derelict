import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { CornucopiaRoutingModule } from './cornucopia-routing.module';
import { CornucopiaComponent } from './cornucopia.component';

@NgModule({
  declarations: [CornucopiaComponent],
  imports: [
    CommonModule,
    CornucopiaRoutingModule
  ]
})
export class CornucopiaModule { }
