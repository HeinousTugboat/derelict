import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { CornucopiaComponent } from './cornucopia.component';

const routes: Routes = [{
  path: '',
  component: CornucopiaComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CornucopiaRoutingModule { }
