import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { CornucopiaComponent } from './cornucopia.component';

describe('CornucopiaComponent', () => {
  let component: CornucopiaComponent;
  let fixture: ComponentFixture<CornucopiaComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ CornucopiaComponent ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CornucopiaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
