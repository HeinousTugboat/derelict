import { Component } from '@angular/core';
import { BotService } from '@game/bot.service';
import { ResourceService } from '@game/resource.service';

@Component({
  selector: 'app-cornucopia',
  templateUrl: './cornucopia.component.html',
  styleUrls: ['./cornucopia.component.scss']
})
export class CornucopiaComponent {
  constructor(public bot: BotService, public resource: ResourceService) { }

  public buildBot(): void {
    if (this.resource.refinedRegolith.current < 10) { return; }

    this.resource.refinedRegolith.current -= 10;
    this.bot.buildBot();
  }

}
