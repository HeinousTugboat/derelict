import { Component } from '@angular/core';
import { ClickService } from '@core/click.service';
import { BotService } from '@game/bot.service';
import { Loop } from '@game/loop.interface';
import { LoopService } from '@game/loop.service';
import { ResourceService } from '@game/resource.service';
import { Bot } from '@models/bot';
import { invalid } from '@utils';

import { ProgressButtonModel } from '../ui/progress-button/progress-button.model';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent {
  public botMenuOpen = false;
  public botList: Set<Bot> = new Set();

  public buttons: ProgressButtonModel[] = [
    new ProgressButtonModel('Numero Uno', 1_000),
    new ProgressButtonModel('Numero Duo', 2_000),
    new ProgressButtonModel('Numero Treo', 3_000),
    new ProgressButtonModel('Numero Quattro', 5_000),
    new ProgressButtonModel('Numero Cinqo', 7_500),
    new ProgressButtonModel('Numero Sixo', 10_000)
  ];

  constructor(
    public bot: BotService,
    public resource: ResourceService,
    public clicks: ClickService,
    loop: LoopService) {
    loop.loop$.subscribe(interval => this.update(interval));
  }

  public start(data: ProgressButtonModel): void {
    data.isRunning = true;
    data.disabled = true;
  }

  public update({ interval }: Loop): void {
    this.buttons.forEach((data) => {
      if (!data.isRunning) { return; }

      data.duration += interval;
      if (data.duration >= data.length) {
        data.isRunning = false;
        data.disabled = false;
        data.duration = 0;
      }
    });
  }

  public focusBotMenu(): void {
    this.botMenuOpen = !this.botMenuOpen;
  }

  public select(choice: Bot | null): void {
    this.botMenuOpen = false;
    if (!invalid(choice)) {
      this.botList.add(choice);
    }
  }
}
