import { MarkdownModule } from 'ngx-markdown';

import { HttpClient, HttpClientModule } from '@angular/common/http';
import { ErrorHandler, ModuleWithProviders, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { GameModule } from '@game/game.module';
import { SharedModule } from '@shared/shared.module';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ChangelogComponent } from './changelog/changelog.component';
import { GlobalErrorHandler } from './core/error-handler';

@NgModule({
  declarations: [
    AppComponent,
    ChangelogComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    AppRoutingModule,
    SharedModule,
    GameModule,
    MarkdownModule.forRoot({ loader: HttpClient }) as ModuleWithProviders<MarkdownModule>
  ],
  providers: [
    { provide: GlobalErrorHandler, useClass: GlobalErrorHandler },
    { provide: ErrorHandler, useExisting: GlobalErrorHandler },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
