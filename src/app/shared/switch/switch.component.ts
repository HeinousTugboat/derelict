import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-switch',
  templateUrl: './switch.component.html',
  styleUrls: ['./switch.component.scss']
})
export class SwitchComponent implements OnInit {
  @Input() label?: string;
  @Output() clicked = new EventEmitter();
  labelId!: string;
  constructor() { }

  public ngOnInit(): void {
    if (this.label === undefined) { throw new Error('Missing Label'); }

    this.labelId = `${this.label}-switch`;
  }

  public onClick(): void { this.clicked.emit(); }
}
