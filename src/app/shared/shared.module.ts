import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { FlyOutMenuComponent } from './fly-out-menu/fly-out-menu.component';
import { ResourceMeterComponent } from './resource-meter/resource-meter.component';
import { SwitchComponent } from './switch/switch.component';

@NgModule({
  declarations: [
    SwitchComponent,
    ResourceMeterComponent,
    FlyOutMenuComponent
  ],
  imports: [
    CommonModule,
    FormsModule
  ],
  exports: [
    CommonModule,
    FormsModule,
    SwitchComponent,
    ResourceMeterComponent,
    FlyOutMenuComponent
  ]
})
export class SharedModule { }
