import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ResourceMeterComponent } from './resource-meter.component';

describe('ResourceMeterComponent', () => {
  let component: ResourceMeterComponent;
  let fixture: ComponentFixture<ResourceMeterComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ResourceMeterComponent ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResourceMeterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
