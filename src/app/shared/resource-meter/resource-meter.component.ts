import { Component, Input } from '@angular/core';
import { Resource } from '@models/resource';

@Component({
  selector: 'app-resource-meter',
  templateUrl: './resource-meter.component.html',
  styleUrls: ['./resource-meter.component.scss']
})
export class ResourceMeterComponent {
  @Input() resource: Resource = Resource.missing;
  constructor() { }
}
