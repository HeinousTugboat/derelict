import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { FlyOutMenuComponent } from './fly-out-menu.component';

describe('FlyOutMenuComponent', () => {
  let component: FlyOutMenuComponent;
  let fixture: ComponentFixture<FlyOutMenuComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ FlyOutMenuComponent ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FlyOutMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
