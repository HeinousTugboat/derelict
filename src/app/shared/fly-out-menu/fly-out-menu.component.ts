import { Subscription } from 'rxjs';
import { filter, skip } from 'rxjs/operators';

import { animate, style, transition, trigger } from '@angular/animations';
import { Component, ElementRef, EventEmitter, Input, OnDestroy, Output } from '@angular/core';
import { ClickService } from '@core/click.service';

@Component({
  selector: 'app-fly-out-menu',
  templateUrl: './fly-out-menu.component.html',
  styleUrls: ['./fly-out-menu.component.scss'],
  animations: [
    trigger('openClose', [
      transition(':enter', [
        style({ opacity: 0 }),
        animate('150ms', style({ opacity: 1 })),
      ]),
      transition(':leave', [
        animate('150ms', style({ opacity: 0 }))
      ])
    ])
  ]
})
export class FlyOutMenuComponent<T> implements OnDestroy {
  @Input() header = 'Fly-Out Menu, dude!';
  @Input() choices: readonly T[] = [];
  @Output() choiceMade = new EventEmitter<T | null>();
  private _isOpen = false;
  private subscription?: Subscription;

  public get isOpen(): boolean { return this._isOpen; }
  @Input() public set isOpen(open: boolean) {
    if (open && this._isOpen !== open) {
      this.subscription = this.clicks.clicks$
        .pipe(
          filter(() => this.isOpen),
          skip(1)
        ).subscribe(click => {
          if (!this.ref.nativeElement.contains(click.target)) {
            this.select(null);
          }
        });
    } else if (!open && this._isOpen !== open) {
      this.unsubscribe();
    }
    this._isOpen = open;
  }

  constructor(private clicks: ClickService, private ref: ElementRef<HTMLElement>) { }

  public unsubscribe(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  public select(choice: T | null): void {
    this.choiceMade.emit(choice);
    this.isOpen = false;
    this.unsubscribe();
  }

  public ngOnDestroy(): void {
    if (this.subscription) {
      this.unsubscribe();
    }
  }
}
