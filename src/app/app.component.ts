import { Observable } from 'rxjs';
import { filter } from 'rxjs/operators';

import { Component, NgZone, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ClickService } from '@core/click.service';
import { GlobalErrorHandler } from '@core/error-handler';
import { KeybindService } from '@core/keybind.service';
import { Loop } from '@game/loop.interface';
import { LoopService } from '@game/loop.service';
import { ProcessorService } from '@game/processor.service';
import { ResourceService } from '@game/resource.service';
import { Resource } from '@models/resource';
import { ClickEvent } from '@utils';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'derelict';
  theme = 'is-error';
  currentTab = 'dashboard';
  loop$: Observable<Loop>;
  clicks$: Observable<ClickEvent>;
  paused = true;
  collapsed: Record<string, boolean> = {
    callout: false,
    vitals: false
  };
  start = new Date();

  delayed = false;

  resources: Resource[] = [];

  constructor(
    private loop: LoopService,
    private resource: ResourceService,
    private processor: ProcessorService,
    private clicks: ClickService,
    private keybind: KeybindService,
    private errors: GlobalErrorHandler,
    private router: Router,
    private zone: NgZone
  ) {
    this.loop$ = loop.loop$;
    this.clicks$ = clicks.clicks$;
    errors.error$.subscribe(() => {
      this.theme = 'is-error';
      // this.zone.run(() => {
      //   this.router.navigateByUrl('error');
      // });
    });

    const keys = ['1', '2', '3', '4', '5'];

    keybind.key$.pipe(
      filter((ev: KeyboardEvent) => ev.type === 'keyup' && keys.includes(ev.key))
    ).subscribe(ev => this.keyup(ev));
  }

  public keyup(ev: KeyboardEvent): void {
    switch (ev.key) {
      case '1': this.router.navigateByUrl('dashboard'); break;
      case '2': this.router.navigateByUrl('warehouse'); break;
      case '3': this.router.navigateByUrl('processing'); break;
      case '4': this.router.navigateByUrl('cornucopia'); break;
      case '5': this.router.navigateByUrl('engineering'); break;
    }
  }


  public ngOnInit(): void {
    this.resources = [...this.resource.resources.keys()];
    if (location.href.includes('file')) {
      this.theme = 'is-filesystem';
    } else if (location.href.includes('local')) {
      this.theme = 'is-local';
    } else {
      this.theme = 'is-remote';
    }
  }

  public changeTheme(label: string): void {
    this.theme = label;
  }

  public pause(): void { this.loop.pause(); this.paused = false; }
  public play(): void { this.loop.play(); this.paused = true; }
  public collapse(target: string): void { this.collapsed[target] = !(this.collapsed[target] ?? false); }
}
