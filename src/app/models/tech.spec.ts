import { Tech, TechCategory } from './tech';

describe('Tech', () => {
  it('should create an instance', () => {
    expect(new Tech('Foo', 'Fie', TechCategory.PROCESSING)).toBeTruthy();
  });
});
