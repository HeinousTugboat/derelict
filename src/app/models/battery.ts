import { invalid } from '@utils';

import { PowerSource } from './power-source';

export const isBattery = (source: PowerSource): source is Battery => !invalid((source as Battery).recharge);

export class Battery extends PowerSource {
  override type = 'battery';

  constructor(
    /* 360 J/g is minimal for Lithum. 45g @ 360J/g = 16k Joules. About the same
    as an 18650. 875 - 1000 is top end. 85kwh battery has like 7,000 of these..
    runs about 80s at 200W */
    public readonly specificEnergy = 360, // J/g
    public readonly mass = 45, // g
    initialCharge = 1
  ) { super(initialCharge); }

  public get capacity(): number { return this.specificEnergy * this.mass; }

  public recharge(j: number): void {
    this.energy += j / this.capacity;
    if (this.energy > 1) {
      this.energy = 1;
    }
  }

  public override toString(): string {
    return `${this.id} [${(100 * this.charge / this.capacity).toFixed(1)}%]`;
  }
}
