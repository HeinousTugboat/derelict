export abstract class PowerSource {
  abstract get capacity(): number;

  protected type = 'source';
  protected ident = Math.floor(Math.random() * 0xFFFFFF).toString(16).padStart(6, '0');

  constructor(protected energy = 1.0) { }

  public get charge(): number { return this.energy * this.capacity; }
  public get id(): string { return `${this.type}-${this.ident}`; }

  public draw(j: number): void {
    this.energy -= j / this.capacity;

    if (this.energy < 0) {
      const charge = this.energy;
      this.energy = 0;
      throw new Error(`PowerSoure ${this.ident} has negatve charge! [${charge}]`);
    }
  }

  public toString(): string {
    return this.id;
  }
}
