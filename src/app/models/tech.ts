import { invalid } from '@utils';

import { Resource } from './resource';
import techs from './tech.json';

export type TechRequirement = Tech | string | [string, number] | [Resource, number];

export type EncryptedTech = Tech;
export type DecryptedTech = Tech;
export type DecryptedData = Set<DecryptedTech>;

/* eslint-disable @typescript-eslint/naming-convention */
export enum TechCategory {
  PROCESSING = 'processing',
  BOT = 'bot',
  BATTERY = 'battery',
  POWER = 'power'
}

export interface TechJson {
  id: string;
  name: string;
  description: string;
  category: string;
  unlocked: boolean;
  decrypted: boolean;
  unlock_requirements: (string | [string, number])[];
  decryption_requirements: (string | [string, number])[];
}
/* eslint-enable @typescript-eslint/naming-convention */

export class Tech {
  static list: Map<TechCategory, Set<Tech>> = new Map();

  private _decrypted = false;
  private _unlocked = false;

  constructor(
    public readonly id: string,
    public readonly name: string,
    public readonly description: string,
    public readonly category: TechCategory,
    private decryptionRequirements: Set<TechRequirement> = new Set(),
    private unlockRequirements: Set<TechRequirement> = new Set()
  ) {
    Tech.get(category).add(this);
  }

  public static load(): readonly Tech[] {
    console.debug('Tech.load');
    if (Tech.list.size === 0) {
      Tech.initialize();
    }
    const loadedTechs = [];
    console.debug('Tech.load', techs);
    for (const tech of techs) {
      loadedTechs.push(Tech.fromJSON(tech));
    }

    return loadedTechs;
  }

  public static fromJSON(json: TechJson): Tech {
    const category: TechCategory | undefined = Object.values(TechCategory).find(x => x === json.category);

    if (invalid(category)) { throw new Error(`Deserialization error: TechCategory invalid ${json.category}`); }

    const tech = new Tech(
      json.id,
      json.name,
      json.description,
      category,
      new Set(json.unlock_requirements),
      new Set(json.decryption_requirements)
    );
    if (json.unlocked) { tech.initialUnlock(); }
    if (json.decrypted) { tech.initialDecrypt(); }

    return tech;
  }

  public static get(category: TechCategory): Set<Tech> {
    if (!Tech.list.has(category)) { throw new Error(`Missing Tech List for Category ${category}`); }

    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    return Tech.list.get(category)!;
  }

  public static initialize(): void {
    console.debug('Tech.init');
    for (const category of Object.values(TechCategory)) {
      Tech.list.set(category, new Set());
    }
  }

  public get decrypted(): boolean {
    return this._decrypted;
  }

  public get encrypted(): boolean {
    return !this._decrypted;
  }

  public get unlocked(): boolean {
    return this._unlocked;
  }

  public decrypt(requirements: TechRequirement[]): boolean {
    this._decrypted = this._unlocked && requirements.every(req => this.decryptionRequirements.has(req));
    return this._decrypted;
  }

  public unlock(requirements: TechRequirement[]): void {
    this._unlocked = requirements.every(req => this.unlockRequirements.has(req));
  }

  protected initialUnlock(): void {
    this._unlocked = true;
  }

  protected initialDecrypt(): void {
    this._decrypted = true;
  }
}
