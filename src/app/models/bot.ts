import { invalid } from '@utils';

import { PowerSource } from './power-source';

/* eslint-disable @typescript-eslint/naming-convention */
export enum BotJob {
  IDLE = 'Idle',
  MINING_REGOLITH = 'Mining'
}
/* eslint-enable @typescript-eslint/naming-convention */

export class Bot {
  protected static id = 0;

  // Admin stuff
  id = `bot-${(Math.floor(Math.random() * 0xFFFF)).toString(16).padStart(4, '0')}`;
  type = 'Basic Bot';
  job = BotJob.IDLE;
  isActive = false;
  isDeactivated = false;
  life = 0;

  // Generation stuff
  power = 200; // Joules/Watt-second
  efficiency = 0.70; // %
  powerCutoff = 0.25; // %

  // Temperature stuff
  mass = 2_000; // g
  temperature = 0; // C
  specificHeatCapacity = 0.42; // J / (g * K)
  minTemperature = -20; // C
  maxTemperature = 120; // C

  heatTransferCoefficient = 2; // Watts / (m^2 * K)
  // ...https://ocw.mit.edu/courses/aeronautics-and-astronautics/16-851-satellite-engineering-fall-2003/lecture-notes/l23thermalcontro.pdf
  emissivity = 0.4; // Watts / (m^2 - K^4)
  area = 1; // m^2

  public get isSafe(): boolean { return this.temperature < this.maxTemperature && this.temperature > this.minTemperature; }
  public get wasteHeat(): number { return this.isActive ? this.power * (1 - this.efficiency) / this.heatCapacity : 0; }
  public get heatCapacity(): number { return this.mass * this.specificHeatCapacity; }
  public get thermalRadiation(): number { return this.emissivity * 5.670e-8 * Math.pow(this.temperature, 4) * this.area; }
  public get temperatureFlux(): number { return this.wasteHeat - this.thermalRadiation; }
  public get jobs(): BotJob[] { return [BotJob.MINING_REGOLITH, BotJob.IDLE]; }

  constructor(public source?: PowerSource) { }
  /**
   * @param dT elapsed milliseconds
   * @returns work in Joules
   */
  public tick(dT: number): number {
    let work = this.power * dT / 1000;

    if (this.job === BotJob.IDLE) {
      work = 0;
    } else if (this.isDeactivated || invalid(this.source) || this.power * this.powerCutoff > this.source.charge) {
      this.isActive = false;
      work = 0;
    } else if (!this.isActive || !this.isSafe) {
      this.isActive = work > 0 && this.isSafe;
      work = 0;
    } else {
      this.source.draw(work);
      this.life += dT;
      this.temperature += work * (1 - this.efficiency) / this.heatCapacity;
    }

    this.radiate(dT);
    return work * this.efficiency;
  }

  public radiate(dT: number): number {
    return this.temperature -= this.thermalRadiation * dT / 1000;
  }

  public convect(dT: number, atmosphere = 0): number {
    // Air? Maybe? Ranges from 0.5 to 10s of thousands..
    const t = this.heatCapacity / (this.heatTransferCoefficient * this.area);
    const t0 = this.temperature;

    this.temperature = atmosphere + (t0 - atmosphere) * Math.exp(-dT / (t * 1000));

    return this.temperature - t0;
  }

  public toString(): string {
    return this.id;
  }
}
