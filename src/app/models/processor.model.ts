import { Resource } from './resource';

/* eslint-disable @typescript-eslint/naming-convention */
export enum ProcessorInputType {
  WORK = 'work',
  MATERIAL = 'material'
}
/* eslint-enable @typescript-eslint/naming-convention */

export class MaterialInput {
  type = ProcessorInputType.MATERIAL;

  constructor(
    public resource: Resource,
    public quantity: number = 1
  ) { }
}

export type ProcessorInput = MaterialInput | ProcessorInputType.WORK;

export class Processor {
  constructor(
    private inputs: ProcessorInput[],
    private output: Resource,
    private ratioWorkDone: number = 1
  ) { }

  public process(): void {
    const willRun = this.inputs.reduce((shouldRun, input) => {
      if (input === ProcessorInputType.WORK
        || input.resource.current < input.quantity
        || this.output.current >= this.output.total
      ) { return false; }
      return shouldRun;
    }, true);

    if (willRun) {
      this.inputs.forEach((input) => {
        if (input === ProcessorInputType.WORK) { return; }

        input.resource.current -= input.quantity;
      });

      this.output.harvest(this.output.workNeeded * this.ratioWorkDone);
    }
  }
}
