import { invalid } from '@utils';

const FACTOR = 1 - 1 / 100;
const TIME_LOOKBACK = 2_500;

export class Resource {
  static missing = new Resource('missing', 'Missing', -1, -1);
  public rate = 0;
  private prevValues: [number, number][] = [];

  constructor(
    public name: string,
    public label: string,
    public current: number,
    public total: number = 0,
    public workNeeded: number = 90,
    public quantityPerHarvest: number = 1
  ) {
    this.prevValues.push([0, this.current]);
  }

  public harvest(work: number): number {
    if (work < this.workNeeded) {
      throw new Error(`Attempting to harvest ${this.name} with less work than needed! [${work}/${this.workNeeded}]`);
    }

    const multipleProduced = Math.floor(work / this.workNeeded);
    const workConsumed = multipleProduced * this.workNeeded;
    let quantityProduced = this.quantityPerHarvest * multipleProduced;

    if (this.total > 0 && this.current + quantityProduced > this.total) {
      quantityProduced = this.total - this.current;
      // workConsumed = quantityProduced * this.workNeeded; // TODO: Should overproduction consume excess work? Maaaaybe.
    }

    this.current += quantityProduced;
    return workConsumed;
  }

  public updateRate(time: number): void {
    this.prevValues.push([time, this.current]);
    const times = this.prevValues.filter(([prevTime]) => prevTime >= time - TIME_LOOKBACK);
    if (times.length < 2) {
      const finalValue = this.prevValues[this.prevValues.length - 2];

      if (invalid(finalValue)) { throw new Error('Attempting to Update Rate with one value'); }

      times.unshift(finalValue);
    }

    const start = times[0];
    const end = times[times.length - 1];

    if (invalid(start) || invalid(end)) { throw new Error('Start and/or End missing from rate calculation'); }

    const newRate = 1000 * (end[1] - start[1]) / (end[0] - start[0]);
    this.rate = FACTOR * this.rate + (1 - FACTOR) * newRate;

    if (Math.abs(this.rate) < 1e-3) {
      this.rate = 0;
    }

    if (Number.isNaN(this.rate)) {
      console.error('rate is NaN!', this, start, end, newRate, times);
      this.rate = 0;
    }
  }

  public toString(): string {
    if (invalid(this.total)) {
      return `${this.name} (${this.current.toFixed(0)}, ${this.rate}/s)`;
    } else {
      return `${this.name} (${this.current.toFixed(0)}/${this.total.toFixed(0)}, ${this.rate}/s)`;
    }
  }
}

