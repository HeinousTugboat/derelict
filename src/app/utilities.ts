export const invalid = <T>(o: T | undefined | null): o is undefined | null => o === undefined || o === null;
export const assertNever = (x: never): never => { throw new Error(`Unexpected value ${x}. Should have been never.`); };

export type ClickEvent = MouseEvent & { target: HTMLElement };
