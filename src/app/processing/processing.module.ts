import { NgModule } from '@angular/core';
import { SharedModule } from '@shared/shared.module';

import { ProcessingRoutingModule } from './processing-routing.module';
import { ProcessingComponent } from './processing.component';

@NgModule({
  declarations: [ProcessingComponent],
  imports: [
    SharedModule,
    ProcessingRoutingModule
  ]
})
export class ProcessingModule { }
