import { fromEvent, Observable } from 'rxjs';

import { Injectable } from '@angular/core';
import { ClickEvent } from '@utils';

@Injectable({
  providedIn: 'root'
})
export class ClickService {
  public readonly clicks$: Observable<ClickEvent>;

  constructor() {
    this.clicks$ = fromEvent(document, 'click') as Observable<ClickEvent>;
  }
}
