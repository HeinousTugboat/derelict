import { fromEvent, merge, Observable } from 'rxjs';

import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class KeybindService {
  public readonly key$: Observable<KeyboardEvent>;
  private keyDown$: Observable<KeyboardEvent>;
  private keyUp$: Observable<KeyboardEvent>;

  constructor() {
    this.keyDown$ = fromEvent(document, 'keydown') as Observable<KeyboardEvent>;
    this.keyUp$ = fromEvent(document, 'keyup') as Observable<KeyboardEvent>;
    this.key$ = merge(this.keyDown$, this.keyUp$);
  }
}
