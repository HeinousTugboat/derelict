import { Observable, Subject } from 'rxjs';

import { ErrorHandler, Injectable } from '@angular/core';

@Injectable()
export class GlobalErrorHandler implements ErrorHandler {
  public error$: Observable<Error>;

  private errorSource$ = new Subject<Error>();

  constructor() {
    this.error$ = this.errorSource$.asObservable();
  }

  public handleError(error: Error): void {
    console.log('Error handled!');
    this.errorSource$.next(error);
    console.error(error);
  }
}
