import { TestBed } from '@angular/core/testing';

import { KeybindService } from './keybind.service';

describe('KeybindService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: KeybindService = TestBed.get(KeybindService);
    expect(service).toBeTruthy();
  });
});
