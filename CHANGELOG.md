# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.2.3](https://gitlab.com/HeinousTugboat/derelict/compare/v0.2.2...v0.2.3) (2021-09-19)


### Features

* **changelog.component:** adds initial component and styling ([8f5863d](https://gitlab.com/HeinousTugboat/derelict/commit/8f5863ddb8ccc794223c8149c9c35a5d80e69a7b)), closes [#22](https://gitlab.com/HeinousTugboat/derelict/issues/22)
* **engineering.component:** updates color schemes to match global theme ([b6da001](https://gitlab.com/HeinousTugboat/derelict/commit/b6da0010c6553a2b1a45a119a203aa57426056f5))
* **error-handler:** adds initial error handling ([54d0d3a](https://gitlab.com/HeinousTugboat/derelict/commit/54d0d3a20b697e358dbd3186bf1eb47188714b9e))
* **keybind.service:** adds basic keybinds to switch tabs ([aa25c97](https://gitlab.com/HeinousTugboat/derelict/commit/aa25c977bd1ccd0e1267825f8f2b41eb00e6b006))
* **progress-button.component:** adds demo logic for tracking+updating duration ([89a3dd3](https://gitlab.com/HeinousTugboat/derelict/commit/89a3dd3b3ad8311b490da959a7f5a5e407de9353)), closes [#25](https://gitlab.com/HeinousTugboat/derelict/issues/25)
* **progress-button.component:** adds initial styling and markup ([bec14a0](https://gitlab.com/HeinousTugboat/derelict/commit/bec14a0fbf8db68f682f8f48d14b27f4408b9ac3)), closes [#25](https://gitlab.com/HeinousTugboat/derelict/issues/25)
* **progress-button.component:** adds model to back progress button, updates dashboard examples ([2c93537](https://gitlab.com/HeinousTugboat/derelict/commit/2c93537c73cc299eaae3033d931264fad054a45e)), closes [#25](https://gitlab.com/HeinousTugboat/derelict/issues/25)
* **tech.model:** moves techs to json, updates engineering pane to dynamically generate ([bc9ae67](https://gitlab.com/HeinousTugboat/derelict/commit/bc9ae67bdd6cf5f1fde1a76cf3b93650a35380f3))


### Bug Fixes

* **app.component:** fixes template possibly-null/undefined issue ([70e2e9f](https://gitlab.com/HeinousTugboat/derelict/commit/70e2e9fe301115d6815d71ec1326e13df17f57d2))
* **app.component:** removes attempt to navigate from error-handler ([c8cdb8c](https://gitlab.com/HeinousTugboat/derelict/commit/c8cdb8c3b7e9d3c484a67461dea8de01f613149c))
* **bot-card.component:** fixes linting errors ([064fffa](https://gitlab.com/HeinousTugboat/derelict/commit/064fffae252b16fc6b8dfc549311cb27ac94344a))
* **bot-card.component:** replaces optional input with not-null assertion, throws error if no bot ([64fc0bd](https://gitlab.com/HeinousTugboat/derelict/commit/64fc0bde220b10969941178eca0ceae727ba4c4c))
* **power-source.model:** add error if power source charge drops below zero ([d8cc177](https://gitlab.com/HeinousTugboat/derelict/commit/d8cc1771d2e822c84eb1ad3b55ff48e1ae535fdb)), closes [#11](https://gitlab.com/HeinousTugboat/derelict/issues/11)

### [0.2.2](https://gitlab.com/HeinousTugboat/derelict/compare/v0.2.1...v0.2.2) (2021-09-02)


### Bug Fixes

* **engineering.component:** stubs out decrypt while service is WIP ([887b941](https://gitlab.com/HeinousTugboat/derelict/commit/887b9418cf98b54c8e7a47031bea6e7f57edf5d6))
* removes inlining of critical styles to work around bug in Angular ([b6af266](https://gitlab.com/HeinousTugboat/derelict/commit/b6af2666e5cb3d3167fd6a59fcbb0e402661b749))

### [0.2.1](https://gitlab.com/HeinousTugboat/derelict/compare/v0.2.0...v0.2.1) (2021-09-02)


### Features

* adds headers to engineering and todo list, small style tweak ([b1cc026](https://gitlab.com/HeinousTugboat/derelict/commit/b1cc0260a5c9959e7f0f9a2843304b469aec35c7))
* adds time game launched to info log ([8ab62cd](https://gitlab.com/HeinousTugboat/derelict/commit/8ab62cde08c49d2f17ab32cc8cacd8eda9c0c5d9))
* **engineering:** adds basic decryption service ([6c8194d](https://gitlab.com/HeinousTugboat/derelict/commit/6c8194da5f7ce69ccafe08881f19fe3d0ad2d292))
* **engineering:** adds first pass at tech model and deserializer ([09d2fbc](https://gitlab.com/HeinousTugboat/derelict/commit/09d2fbc3b076c9bf92757ce698e013e5527436aa))
* **styles:** adds initial monospace fonts, updates fonts for offline use ([d2ddda7](https://gitlab.com/HeinousTugboat/derelict/commit/d2ddda7851e229fbc4a7c22ff8d2ac93a08688fd))


### Bug Fixes

* **dashboard:** fixes typo ([90c49a2](https://gitlab.com/HeinousTugboat/derelict/commit/90c49a21120ba4eb5659f382d3479fed161ac0d7))
* **scss:** adds missing % signs in sass files ([8b7a8d2](https://gitlab.com/HeinousTugboat/derelict/commit/8b7a8d2995c85d68d92e2be1e121172e6b1c2c21))
* update engineering tab's active style ([181b3bf](https://gitlab.com/HeinousTugboat/derelict/commit/181b3bfe1990209fd3d052eb58dd4c4b018febc5))
* updates tsconfig to fix lazy loaded routes ([ab2a69f](https://gitlab.com/HeinousTugboat/derelict/commit/ab2a69f7bec1d9663d6c7518aa497f460984475a))

<a name="0.2.0"></a>
# [0.2.0](https://gitlab.com/HeinousTugboat/derelict/compare/v0.1.0...v0.2.0) (2019-03-19)


### Bug Fixes

* adds test:vsc script to package.json ([8c4e582](https://gitlab.com/HeinousTugboat/derelict/commit/8c4e582))
* bots correctly have activeate/deactivate buttons ([ee05bce](https://gitlab.com/HeinousTugboat/derelict/commit/ee05bce))
* changes resource-meter to display rates that aren't 0 ([7153909](https://gitlab.com/HeinousTugboat/derelict/commit/7153909))
* changes rxjs imports to lowercase ([93f92c9](https://gitlab.com/HeinousTugboat/derelict/commit/93f92c9))
* corrects imports for core files ([513b5ab](https://gitlab.com/HeinousTugboat/derelict/commit/513b5ab))
* fix linear-gradient issue with scss eating % symbol ([367996b](https://gitlab.com/HeinousTugboat/derelict/commit/367996b))
* **tabs:** adds fix if nav not present ([1f610a5](https://gitlab.com/HeinousTugboat/derelict/commit/1f610a5))
* fix: locks rxjs to ^5.5, 6.0 breaks everything for now ([978713c](https://gitlab.com/HeinousTugboat/derelict/commit/978713c))
* **battery.model:** updates string math ([70c2ccf](https://gitlab.com/HeinousTugboat/derelict/commit/70c2ccf))
* fixes incorrectly capitalized imports, sets tsconfig back to es6 ([d6462b8](https://gitlab.com/HeinousTugboat/derelict/commit/d6462b8))
* **bot.model:** separates idle logic from isActive logic ([b84005d](https://gitlab.com/HeinousTugboat/derelict/commit/b84005d))
* fixes build-a-bot button to be disabled instead of hidden if unavailable ([9a908a7](https://gitlab.com/HeinousTugboat/derelict/commit/9a908a7))
* fixes url-based styling ([0e23e13](https://gitlab.com/HeinousTugboat/derelict/commit/0e23e13))
* lowercases rxjs/operators to try and fix ci ([f3b71a2](https://gitlab.com/HeinousTugboat/derelict/commit/f3b71a2))
* one last, tiny change ([02ff886](https://gitlab.com/HeinousTugboat/derelict/commit/02ff886))
* updates deps, corrects webpack config for both server and build ([17621d0](https://gitlab.com/HeinousTugboat/derelict/commit/17621d0))
* updates loop.service to correctly pass time instead of tick as loop$.value ([464a62b](https://gitlab.com/HeinousTugboat/derelict/commit/464a62b))
* updates resource to start at current level for previous values instead of 0 ([a452fc9](https://gitlab.com/HeinousTugboat/derelict/commit/a452fc9))
* **loop.service:** adds delay buffer to core loop ([a578d2f](https://gitlab.com/HeinousTugboat/derelict/commit/a578d2f)), closes [#9](https://gitlab.com/HeinousTugboat/derelict/issues/9)
* updates tsconfig to target es6 so we have access to all the right features ([2547191](https://gitlab.com/HeinousTugboat/derelict/commit/2547191))
* **loop.service:** updates delay code to proper speed up loop, converts time tracking to relative in ([955a50b](https://gitlab.com/HeinousTugboat/derelict/commit/955a50b)), closes [#9](https://gitlab.com/HeinousTugboat/derelict/issues/9) [#10](https://gitlab.com/HeinousTugboat/derelict/issues/10)
* **resource.model:** fixes updateRate to account for long history ([de5dfac](https://gitlab.com/HeinousTugboat/derelict/commit/de5dfac)), closes [#12](https://gitlab.com/HeinousTugboat/derelict/issues/12)
* **scss:** fixes grid row wonkiness with log ([74e9cc9](https://gitlab.com/HeinousTugboat/derelict/commit/74e9cc9))
* **server:** updates log message on start ([d273ae3](https://gitlab.com/HeinousTugboat/derelict/commit/d273ae3))


### Features

* adds asset copy to webpack config ([c0a29c4](https://gitlab.com/HeinousTugboat/derelict/commit/c0a29c4))
* adds assets from derelict-old ([dbdaf92](https://gitlab.com/HeinousTugboat/derelict/commit/dbdaf92))
* adds basic jobs to bot model ([8877521](https://gitlab.com/HeinousTugboat/derelict/commit/8877521))
* adds basic lists of items to engineering and processing tabs ([c43dd12](https://gitlab.com/HeinousTugboat/derelict/commit/c43dd12))
* adds basic processing steps to processing component, and a couple more references ([c0fc4cc](https://gitlab.com/HeinousTugboat/derelict/commit/c0fc4cc))
* adds better smoothing to resource-meter ([f8289a4](https://gitlab.com/HeinousTugboat/derelict/commit/f8289a4))
* adds boatload of svgs ([4c7bf4d](https://gitlab.com/HeinousTugboat/derelict/commit/4c7bf4d))
* adds bot-card for debugging ([695e876](https://gitlab.com/HeinousTugboat/derelict/commit/695e876))
* **bot.model:** moves power source to bot constructor ([1a9c71a](https://gitlab.com/HeinousTugboat/derelict/commit/1a9c71a))
* **cornucopia:** adds empty cornucopia component ([a2b50d4](https://gitlab.com/HeinousTugboat/derelict/commit/a2b50d4))
* **fly-out-menu.component:** adds empty-list handling ([009fa48](https://gitlab.com/HeinousTugboat/derelict/commit/009fa48))
* **fly-out-menu.component:** adds initial buildout of fly out menu ([ec52038](https://gitlab.com/HeinousTugboat/derelict/commit/ec52038))
* adds preliminary, unused core module ([42f2ea3](https://gitlab.com/HeinousTugboat/derelict/commit/42f2ea3))
* **fly-out-menu.component:** updates fly out menu to hook into global click handler ([a77fec1](https://gitlab.com/HeinousTugboat/derelict/commit/a77fec1)), closes [#16](https://gitlab.com/HeinousTugboat/derelict/issues/16)
* adds BrowserAnimationsModule ahead of adding animations ([52912fd](https://gitlab.com/HeinousTugboat/derelict/commit/52912fd))
* adds build+watch npm scripts ([79a2357](https://gitlab.com/HeinousTugboat/derelict/commit/79a2357))
* adds click.service to core and supporting bits ([be2365a](https://gitlab.com/HeinousTugboat/derelict/commit/be2365a)), closes [#16](https://gitlab.com/HeinousTugboat/derelict/issues/16)
* adds complete and error handlers to clicks$ and loop$ streams ([c2b3c23](https://gitlab.com/HeinousTugboat/derelict/commit/c2b3c23))
* adds copy-webpack-plugin for static assets ([1f7ed85](https://gitlab.com/HeinousTugboat/derelict/commit/1f7ed85))
* adds core sass file to pages entry point ([e3a786f](https://gitlab.com/HeinousTugboat/derelict/commit/e3a786f))
* adds cornucopia module ([f7874e6](https://gitlab.com/HeinousTugboat/derelict/commit/f7874e6))
* adds debug message logs, log timestamps and date-fns dep ([702c9d2](https://gitlab.com/HeinousTugboat/derelict/commit/702c9d2))
* adds default toStrings to models ([6a1d0b6](https://gitlab.com/HeinousTugboat/derelict/commit/6a1d0b6))
* adds engineering and processing modules ([3024449](https://gitlab.com/HeinousTugboat/derelict/commit/3024449))
* adds game/loop.ts ([db2e4ce](https://gitlab.com/HeinousTugboat/derelict/commit/db2e4ce))
* adds initial webpack config and adds output to gitignore ([ba66bca](https://gitlab.com/HeinousTugboat/derelict/commit/ba66bca))
* adds power.service and some UI around batteries and sources ([257d068](https://gitlab.com/HeinousTugboat/derelict/commit/257d068))
* adds preliminary mining+refining+bot building.. very preliminary... ([6c0acee](https://gitlab.com/HeinousTugboat/derelict/commit/6c0acee))
* adds resource model ([882f9fb](https://gitlab.com/HeinousTugboat/derelict/commit/882f9fb))
* adds resource-meter to shared module and example to dashboard component ([4278c1e](https://gitlab.com/HeinousTugboat/derelict/commit/4278c1e))
* adds sample spinners ([7494fcc](https://gitlab.com/HeinousTugboat/derelict/commit/7494fcc))
* adds sass loaders to webpack config ([9dd7375](https://gitlab.com/HeinousTugboat/derelict/commit/9dd7375))
* adds selectable sources and jobs to bot card ([7873f15](https://gitlab.com/HeinousTugboat/derelict/commit/7873f15))
* adds set to dashboard of selected bots ([5774fb8](https://gitlab.com/HeinousTugboat/derelict/commit/5774fb8))
* adds tsconfig ([4f1ef93](https://gitlab.com/HeinousTugboat/derelict/commit/4f1ef93))
* adds utilities file and invalid util ([9fd5afd](https://gitlab.com/HeinousTugboat/derelict/commit/9fd5afd))
* adds webpack and assorted packages to package.json ([8dab8dd](https://gitlab.com/HeinousTugboat/derelict/commit/8dab8dd))
* adjusts spacing on fly-out-menu ([49e2a78](https://gitlab.com/HeinousTugboat/derelict/commit/49e2a78))
* changes reference for css output ([868af94](https://gitlab.com/HeinousTugboat/derelict/commit/868af94))
* cleaned up theme switches, add missing ones ([d78bafd](https://gitlab.com/HeinousTugboat/derelict/commit/d78bafd))
* converts app.component back to html, added switch, added theme logic ([fdc97d8](https://gitlab.com/HeinousTugboat/derelict/commit/fdc97d8))
* converts dashboard and bot modules to lazy load ([4e8a758](https://gitlab.com/HeinousTugboat/derelict/commit/4e8a758))
* converts remaining js files to ts files ([38dd95f](https://gitlab.com/HeinousTugboat/derelict/commit/38dd95f))
* copies over initial markup and styles ([fbd5a7e](https://gitlab.com/HeinousTugboat/derelict/commit/fbd5a7e))
* first pass at core game models and BotService ([eabbf6f](https://gitlab.com/HeinousTugboat/derelict/commit/eabbf6f))
* improves performance of messages, removes old defaults ([e4bb5c1](https://gitlab.com/HeinousTugboat/derelict/commit/e4bb5c1))
* initial rewrite of core files to TS from AMD/JS ([f910fc1](https://gitlab.com/HeinousTugboat/derelict/commit/f910fc1))
* makes collapse-arrows functional ([a7feb0b](https://gitlab.com/HeinousTugboat/derelict/commit/a7feb0b))
* modify view to use bundle script instead of loader scripts ([49c0080](https://gitlab.com/HeinousTugboat/derelict/commit/49c0080))
* moves asset files to correct new location ([44dc1e3](https://gitlab.com/HeinousTugboat/derelict/commit/44dc1e3))
* moves build bot button to cornucopia, corrects cornucopia routing ([ee40e17](https://gitlab.com/HeinousTugboat/derelict/commit/ee40e17))
* moves resource indicators back to original section ([ec003e0](https://gitlab.com/HeinousTugboat/derelict/commit/ec003e0))
* moves streams and ClickEvent type to ui/streams ([f7b0835](https://gitlab.com/HeinousTugboat/derelict/commit/f7b0835))
* moves tabs to separate modules ([c93aaa2](https://gitlab.com/HeinousTugboat/derelict/commit/c93aaa2))
* refactor app component to use new resource-meter ([158db8d](https://gitlab.com/HeinousTugboat/derelict/commit/158db8d))
* rejiggers the battery/temperature simulation to be more accurate ([dff35a7](https://gitlab.com/HeinousTugboat/derelict/commit/dff35a7))
* removes default bot, instead starts at 10 refined regolith ([a95fbab](https://gitlab.com/HeinousTugboat/derelict/commit/a95fbab))
* updates bot and bot.service ([5a4f6de](https://gitlab.com/HeinousTugboat/derelict/commit/5a4f6de))
* updates how resource rates are calculated using real rolling windows ([2acb5a7](https://gitlab.com/HeinousTugboat/derelict/commit/2acb5a7))
* **game:** adds loop.service and friends ([660696c](https://gitlab.com/HeinousTugboat/derelict/commit/660696c))
* **resource.service:** adds initial resource service ([8961c6b](https://gitlab.com/HeinousTugboat/derelict/commit/8961c6b))
* **scss:** updates old rules disabled because of FF52 ([7063fec](https://gitlab.com/HeinousTugboat/derelict/commit/7063fec))
* **views/test:** adds test view for narrative experimentation ([744fbab](https://gitlab.com/HeinousTugboat/derelict/commit/744fbab))
